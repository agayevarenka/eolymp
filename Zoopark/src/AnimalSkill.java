public interface AnimalSkill {
     void sound();
     void eating();
     void playing();
     void movement();
}
