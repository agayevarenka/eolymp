import java.util.Scanner;

public class e8955 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long array[] = new long[n];
        for (int i = 0; i < n; i++) {
            array[i] = sc.nextInt();
        }
        boolean positiveElement = false;

        for (int i = 0; i < n; i++) {
            if (array[i] > 0) {
                System.out.print(array[i] + " ");
                 positiveElement = true;
            }
            if (!positiveElement ) {
                System.out.println("NO");
            }
        }
    }
}