import java.util.Scanner;

public class e2863 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        for (int i = 1; i * i <= n; i++) {
            if (n % i == 0 && i % 2 != 0) {
                System.out.print(i + " ");
                if (i != n / i) {
                    int j = n / i;
                    if (j % 2 != 0) {
                        System.out.print(j + " ");
                    }
                }
            }
        }
    }
}
