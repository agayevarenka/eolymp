import java.util.Scanner;

public class e8987 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word1 = sc.nextLine();
        String word2 = word1.replace("a", "@")
                .replace("b", "a")
                .replace("@" , "b");
        System.out.println(word2);
    }
}
