import java.util.Scanner;

public class eMax {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); // sira
        int m = sc.nextInt(); //setir
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        int[] rowSums = new int[n];
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < m; j++) {
                sum += array[i][j];
            }
            rowSums[i] = sum;
        }

        int maxSum = Integer.MIN_VALUE;
        for (int i = 0; i < n; i++) {
            if (rowSums[i] > maxSum) {
                maxSum = rowSums[i];
            }
        }

        for (int i = 0; i < n; i++) {
            if (rowSums[i] == maxSum) {
                System.out.println(i);
            }
        }
    }
}

