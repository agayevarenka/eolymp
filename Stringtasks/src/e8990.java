import java.util.Scanner;

public class e8990 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String word1 = sc.nextLine();

        StringBuilder world2 = new StringBuilder();
        for (int i = 0; i < word1.length(); i++) {
            char c = word1.charAt(i);
            if (c == 'a' || c == 'e' || c == 'i' || c == 'o' || c == 'u' || c == 'y') {
                world2.append(c);
            }
            world2.append(c);
        }

        System.out.println(world2);
    }
}
