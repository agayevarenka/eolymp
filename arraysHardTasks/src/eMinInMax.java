import java.util.Arrays;
import java.util.Scanner;

public class eMinInMax {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int numRows = sc.nextInt();
        int numCols = sc.nextInt();
        int[][] array = new int[numRows][numCols];
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < numRows; i++) {
            for (int j = 0; j < numCols; j++) {
                System.out.print(array[i][j] + " ");
            }
            System.out.println();
        }
        int minOfMax =  Integer.MAX_VALUE;
        for (int i =0; i<numRows; i++) {
            int max=Integer.MIN_VALUE;
            for (int j = 0; j<numCols; j++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }
            }
            if (max < minOfMax) {
                minOfMax = max;
            }

        }
        System.out.println(minOfMax);

    }
}
