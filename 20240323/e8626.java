package turing;

import java.util.Scanner;

public class e8626 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int num1 = n / 1000;
        int num2 = n / 100 % 10;
        int num3 = n / 10 % 10;
        int num4 = n % 10;
        if (num1==3 && num2==7 || num2==3 && num3==7 || num3==3 && num4==7) {
            System.out.println("YES");
        }else {
            System.out.println("NO");
        }

    }
}
