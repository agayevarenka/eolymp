public class Bird extends Animal implements AnimalSkill {
    public void sound() {
        System.out.println("myaw-myaw");
    }

    public void eating() {
        System.out.println("eating");
    }

    public void playing() {
        System.out.println("playing");
    }

    public void movement() {
        System.out.println("flying");
    }

    public Bird(String name, int age) {
        super(name, age);
//        this.voice=Voice.singing;
//        this.movement=Movement.flying;
    }
//    public void sing(){
//        System.out.println("singing");
//    }
//    public void fly() {
//        System.out.println("flying");
//    }
//    public void sleep(){
//        System.out.println("sleeping");
//    }
//    public String getName(){
//        return this.name;
//    }
//    public int getAge() {
//        return this.age;
//    }
//    public  void nest() {
//        System.out.println("nesting");
//    }
//
//    @Override
//    public void eating() {
//        System.out.println("eating");
//    }
//    @Override
//    public void playing() {
//        System.out.println("playing");
//    }
//    @Override
//    public void sound() {
//        System.out.println("singing");
//    }
//    @Override
//    public void movement() {
//        System.out.println("flying");
//    }
}
