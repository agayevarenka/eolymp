import java.util.Arrays;
import java.util.Scanner;

public class e4101 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        long array [] =new long[n];
        for (int i =0; i<n;i++) {
            array [i] = sc.nextInt();
        }
        System.out.println(Arrays.toString(array));
    }
}
