package turing;

import java.util.Scanner;

public class e1623 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        int c = sc.nextInt();
        if (a%2==0 || b%2==0 || c %2==0) {
            System.out.println("YES");
        } else {
            System.out.println("NO");
        }

    }
}
