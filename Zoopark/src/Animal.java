public abstract class Animal {
    String name;
    int age;
    Eyecolor eyecolor;
    //Voice voice;
    //  Movement movement;

//    public void eating() {
//    }
//
//    public void playing() {
//    }
//
//    public void sound() {
//    }
//
//    public void movement() {
//    }

    public Animal(String name, int age) {
        this.name = name;
        this.age = age;
    }

    enum Eyecolor {
        blue,
        brown,
        purple,
        green,
        black,
        yellow
    }
//    enum Voice{
//        hawhaw,
//        myawmyaw,
//        singing
//    }
//    enum Movement {
//        running,
//        jumping,
//        flying
//    }
}
