package turing;

import java.util.Scanner;

public class e902 {
    public static void main(String[] args) {
        Scanner sc=new Scanner(System.in);
        int n= sc.nextInt();
        if (1<=n && n<=3){
            System.out.println("Initial");
        } else if (4<=n && n<=6) {
            System.out.println("Average");
        } else if (7<=n && n<=9) {
            System.out.println("Sufficient");
        } else if (10<=n && n<=12){
            System.out.println("High");
        }
    }
}
