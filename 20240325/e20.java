import java.util.Scanner;

public class e20 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int counter = 0;

        while (a > 0) {
            a -= calculateSum(a);
            counter++;
        }

        System.out.println(counter);
    }

    public static int calculateSum(int a) {
        int temp = a;
        int sum = 0;
        while (temp > 0) {
            sum += temp % 10;
            temp /= 10;
        }
        return sum;

    }
}