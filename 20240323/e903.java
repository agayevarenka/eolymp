package turing;

import java.util.Scanner;

public class e903 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int num1 = n / 100;
        int num2 = n / 10 % 10;
        int num3 = n % 100 % 10;
        if (num1 > num2 && num1 > num3) {
            System.out.println(num1);
        } else if (num2 > num1 && num2 > num3) {
            System.out.println(num2);
        } else if (num3 > num1 && num3 > num2){
            System.out.println(num3);
        }else{
            System.out.println("=");
        }

    }
}
