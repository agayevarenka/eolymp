import java.util.Scanner;

public class eMin {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt(); // sira
        int m = sc.nextInt(); //setir
        int[][] array = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        int[] nsum = new int[n];
        for (int i = 0; i < n; i++) {
            int sum = 0;
            for (int j = 0; j < m; j++) {
                sum += array[i][j];

            }
            nsum[i] = sum;
        }
        int minSum = Integer.MAX_VALUE;
        for (int i = 0; i < n; i++) {
            if (nsum[i] < minSum) {
                minSum = nsum[i];
            }
        }

        for (int i = 0; i < n; i++) {
            if (nsum[i] == minSum) {
                System.out.println(i+1);
            }
        }
    }
}
