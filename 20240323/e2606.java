package turing;

import java.util.Scanner;

public class e2606 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        int b = sc.nextInt();
        if (a > b) {
            System.out.println(b + " " + a);
        } else if (b > a) {
            System.out.println(a + " " + b);
        }

    }
}
