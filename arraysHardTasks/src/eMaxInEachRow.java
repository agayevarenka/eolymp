import java.util.Scanner;

public class eMaxInEachRow {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int m = sc.nextInt();
        int array[][] = new int[n][m];
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                array[i][j] = sc.nextInt();
            }
        }
        for (int i = 0; i < n; i++) {
            for (int j = 0; j < m; j++) {
                System.out.print(array[i][j] + " ");
            }
        }

        int[] maxInColumns = new int[m];
        for (int j = 0; j < m; j++) {
            int max = Integer.MIN_VALUE;
            for (int i = 0; i < n; i++) {
                if (array[i][j] > max) {
                    max = array[i][j];
                }
            }
            maxInColumns[j] = max;
        }
        for (int j = 0; j < m; j++) {
            System.out.print( j + maxInColumns[j]);
        }

    }

}

