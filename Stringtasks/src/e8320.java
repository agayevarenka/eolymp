import java.util.Scanner;

public class e8320 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String str = sc.nextLine();
        String[] words = str.split(" ");
        String result= " ";
        for (String  currentWord:words){
            if (!currentWord.isBlank()){
                result+=currentWord.substring(0,1).toUpperCase() + currentWord.substring(1) + " ";
            }else {
                result+=" ";
            }
        }
        for (int i = 0; i < words.length; i++) {
            words[i] = words[i].substring(0, 1).toUpperCase() + words[i].substring(1);
        }
    }
}
